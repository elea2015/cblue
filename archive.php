<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>
			<?php 

			$the_Term_ID = get_queried_object()->term_id;
			$term = get_term( $the_Term_ID );
			$termTitle = $term->name;

			//$locationHeading = preg_replace('/[^\p{L}\p{N}\s]/u', ' ', $_GET["location"]);
	
			?>
			
			</div>

			<h1 class="text-center locationHeading"><?php echo $termTitle; ?></h1>

			<?php if (have_posts()):  ?>

			<section class="buySection buyListing">
					<div class="container">
				
					   <div class="grid2">
					     <?php  while (have_posts()) : the_post(); $a++;?>
					        
					        	<?php 
					        		$propertyLink = get_post_permalink();
					        		$propertyId =	get_the_ID();

					        		$time = get_field('time');

					        		$allposttags = get_the_tags();
					                $i=0;
					                if ($allposttags) {
					                    foreach($allposttags as $tags) {
					                        $i++;
					                        if (1 == $i) {
					                            $firsttag = $tags->name;
					                        }
					                    }
					                }
					        	 ?>
					          	<!-- Price Card -->
								<div class="card grid-item grid-item--width2 transition">
									<a href="<?php echo $propertyLink; ?>">
										<div class="card-img-top-container">
											<img class="card-img-top" src="<?php the_post_thumbnail_url('custom-size'); ?>" alt="Card image cap">
										</div>
										<h5><span class="badge badge-primary"><?php echo get_post_type(); ?></span></h5>
										<div class="card-body">
											<h4 class="card-title"><?php the_title(); ?></h4>
											<h6 class="card-subtitle mb-2 text-muted"><?php echo get_the_term_list( $propertyId, 'location'); ?></h6>
											<p class="card-text">$<?php echo number_format(get_field('price'));?><?php if ( $time == 'Month' ): echo " /Month"; elseif ( $time == 'Night' ): echo " /Night"; elseif ( $time == 'Week' ): echo " /Week"; endif; ?></p>
											<span class="card-link disabled"><?php echo get_field('bedrooms')?> <i class="fa fa-bed" aria-hidden="true"></i></span>
											<span class="card-link disabled"><?php echo get_field('bathrooms')?> <i class="fa fa-bath" aria-hidden="true"></i></span>
										</div>
									</a>
								</div>
								<!-- end price Card -->
					        
					     <?php  //if($a % 3 === 0) :  echo '</div> <div class="row priceRow">'; endif;
					      ?>
						<?php endwhile; endif ?>
						</div>
					</div>
				</div>
				</section>

<?php get_footer(); ?>
