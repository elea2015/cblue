<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section style="margin:100px auto" class="container">
			<div class="row">
				<div class="col-md-8">
					<h1><?php _e( 'Categories for ', 'html5blank' ); single_cat_title(); ?></h1>
					<?php get_template_part('loop'); ?>
					<?php get_template_part('pagination'); ?>
				</div>
				<div class="col-md-4"><?php get_sidebar(); ?></div>
			</div>
		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
