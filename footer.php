			<!-- footer -->
			<footer class="footer" role="contentinfo">
				<div class="container">
					<div class="row">
						<div class="col-md">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo-footer.svg" alt="Logo" class="logo-img">
							<!-- <p>CanaBlue</p> -->
							<p>Bávaro, Punta Cana.
							</p>
							<ul class="socialFooter list-inline">
								<li class="list-inline-item"><a target="_blank" href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwjUvMW39d3YAhUV9WMKHZIeBnoQFggnMAA&url=https%3A%2F%2Fwww.facebook.com%2Fcanablue%2F&usg=AOvVaw0KwCs4Y8hcQNZGUGpmFZyq"><i class="fa fa-facebook"></i></a></li>
								<li class="list-inline-item"><a target="_blank" href="https://www.instagram.com/cana_blue_punta_cana/"><i class="fa fa-instagram"></i></a></li>
								<li class="list-inline-item"><a target="_blank" href="https://www.linkedin.com/company/canablue"><i class="fa fa-linkedin"></i></a></li>
							</ul>
							<p class="copyright">&copy; <?php echo date('Y'); ?> Copyright <?php bloginfo('name'); ?></p>
						</div>
						<div class="col-md">
							<h5>Company</h5>
							<hr>
							<ul class="list-unstyled">
								<li><a href="/?page_id=279">Meet the Team</a></li>
								<li><a href="/?page_id=2677">Blog</a></li>
								<li><a href="#" data-toggle="modal" data-target="#contactModal">Contact Us</a></li>
							</ul>
						</div>
						<div class="col-md">
							<h5>Services</h5>
							<hr>
							<ul class="list-unstyled">
								<li><a href="<?php echo home_url(); ?>/?page_id=10">Properties for Sale</a></li>
								<li><a href="<?php echo home_url(); ?>/?page_id=94">Sell</a></li>
								<li><a href="<?php echo home_url(); ?>/?page_id=19">Rentals</a></li>
								<li><a href="<?php echo home_url(); ?>/?page_id=106">Legal</a></li>
								<li><a href="<?php echo home_url(); ?>/?page_id=108">Property Management</a></li>
								<li><a href="<?php echo home_url(); ?>/?page_id=108">Renovations</a></li>
							</ul>
						</div>
						<div class="col-md">
							<h5>Neighborhoods</h5>
							<hr>
							<ul class="list-unstyled">
								<li><a href="<?php echo home_url(); ?>/location/palma-real-villas-cocotal">Palma Real Villas (Cocotal)</a></li>
								<li><a href="<?php echo home_url(); ?>/location/costa-bavaro">Costa Bavaro</a></li>
								<li><a href="<?php echo home_url(); ?>/location/el-cortecito/">El Cortecito</a></li>
								<li><a href="<?php echo home_url(); ?>/location/white-sands">White Sands</a></li>
								<li><a href="<?php echo home_url(); ?>/location/cap-cana">Cap Cana</a></li>
								<li><a href="<?php echo home_url(); ?>/location/punta-cana">Punta Cana</a></li>

							</ul>
						</div>
					</div>
				</div>
			</footer>
			<!-- /footer -->
			<!-- Modal -->
			<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="contactModalLabel">Contact us</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body contactForm">
			        <?php
					$property_field = get_the_title();
					echo do_shortcode('[gravityform id="4" field_values="property='.$property_field.'" title="false" description="false"]');
					?>
			      </div>
			      <div class="mfooter">
			        <h5>Pamela Nuñez</h5>
					<p><i class="fa fa-mobile"></i> 809-975-3900 </p>
					<a href="#"><i class="fa fa-envelope-o"></i> pamela@canablue.com</a>
			      </div>
			    </div>
			  </div>
			</div>
		
		<?php wp_footer(); ?>
		<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/node_modules/owl.carousel/dist/owl.carousel.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/myscript.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lightbox/js/lightbox.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js"></script>
		<script>
              new WOW().init();
              </script>
		<script type="text/javascript">

		</script>

	</body>
</html>
