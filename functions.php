<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{ 
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 400, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');
    add_image_size( 'property-screenshot', 800, 500, array( 'left', 'top' ) );
    add_image_size('slider-size', 1920, 950, array( 'left', 'center' ));
    add_image_size('cards', 550, 300, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');
    add_image_size('feature', 330, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'navbar-nav ml-auto',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

    	wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('conditionizr'); // Enqueue it!

        wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        wp_enqueue_script('modernizr'); // Enqueue it!

        wp_register_script('html5blankscripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('html5blankscripts'); // Enqueue it!
    }
}
//Load footer script

function canablue_footer_script()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        wp_register_script('jquerycustom', get_template_directory_uri() . '/js//lib/jquery.min.js', array('jquery'), '2.2.4'); // Custom scripts
        wp_enqueue_script('jquerycustom'); // Enqueue it!

        wp_register_script('b2', get_template_directory_uri() . '/js/lib/popper.min.js', array('jquery'), '1.12.3'); // Custom scripts
        wp_enqueue_script('b2'); // Enqueue it!

        wp_register_script('bootstrapjs', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('bootstrapjs'); // Enqueue it!
    }
}

// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts()
{
    if (is_page('pagenamehere')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname'); // Enqueue it!
    }
}

// Load HTML5 Blank styles
function html5blank_styles()
{
    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!

    wp_register_style('html5blank', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('html5blank'); // Enqueue it!
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
// function html5_blank_view_article($more)
// {
//     global $post;
//     return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
// }

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

//Footer
add_action( 'wp_footer', 'canablue_footer_script' );

/*------------------------------------*\
	Custom Post Resale
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_html5()
{
    register_taxonomy_for_object_type('category', 'buy'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'buy');
    register_post_type('buy', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Resales', 'html5blank'), // Rename these to suit
            'singular_name' => __('Resale', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New Resale', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit Resale', 'html5blank'),
            'new_item' => __('New Resale', 'html5blank'),
            'view' => __('View Resale', 'html5blank'),
            'view_item' => __('View Resale', 'html5blank'),
            'search_items' => __('Search Resale', 'html5blank'),
            'not_found' => __('No Resales found', 'html5blank'),
            'not_found_in_trash' => __('No Resales found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'author'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        // 'rewrite' => array(
        //         'slug' => 'resale'
        // ),
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}

/*------------------------------------*\
    Custom Post Rentals
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_resale()
{
    register_taxonomy_for_object_type('category', 'rent'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('location', 'rent'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'rent');
    register_post_type('rent', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Rentals', 'html5blank'), // Rename these to suit
            'singular_name' => __('Rental', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New Rental', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit Rental', 'html5blank'),
            'new_item' => __('New Rental', 'html5blank'),
            'view' => __('View Rental', 'html5blank'),
            'view_item' => __('View Rental', 'html5blank'),
            'search_items' => __('Search Rental', 'html5blank'),
            'not_found' => __('No Rentals found', 'html5blank'),
            'not_found_in_trash' => __('No Rentals found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'author'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}

add_action('init', 'create_post_type_resale');


/*------------------------------------*\
    Custom Post New-Dev
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_new()
{
    register_taxonomy_for_object_type('category', 'newdev'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'newdev');
    register_post_type('newdev', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Developments', 'html5blank'), // Rename these to suit
            'singular_name' => __('Development', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New Development', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit Development', 'html5blank'),
            'new_item' => __('New Development', 'html5blank'),
            'view' => __('View Development', 'html5blank'),
            'view_item' => __('View Development', 'html5blank'),
            'search_items' => __('Search Development', 'html5blank'),
            'not_found' => __('No Developments found', 'html5blank'),
            'not_found_in_trash' => __('No Developments found in Trash', 'html5blank'),
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'author'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'rewrite' => array(
                'slug' => 'new'
        ),
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support

    ));
}

add_action('init', 'create_post_type_new');


// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_first_home()
{
    register_taxonomy_for_object_type('category', 'newdev'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'newdev');
    register_post_type('first-home', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('First Homes', 'html5blank'), // Rename these to suit
            'singular_name' => __('First Home', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New First Home', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit First Home', 'html5blank'),
            'new_item' => __('New First Home', 'html5blank'),
            'view' => __('View First Home', 'html5blank'),
            'view_item' => __('View First Home', 'html5blank'),
            'search_items' => __('Search First Home', 'html5blank'),
            'not_found' => __('No First Homes found', 'html5blank'),
            'not_found_in_trash' => __('No First Homes found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'author'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}

add_action('init', 'create_post_type_first_home');

// Create 1 Custom Post Lots
function create_post_type_lot()
{
    register_taxonomy_for_object_type('category', 'lot'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'lot');
    register_post_type('lot', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Lots', 'html5blank'), // Rename these to suit
            'singular_name' => __('Lot', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New Lot', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit Lot', 'html5blank'),
            'new_item' => __('New Lot', 'html5blank'),
            'view' => __('View Lot', 'html5blank'),
            'view_item' => __('View Lot', 'html5blank'),
            'search_items' => __('Search Lot', 'html5blank'),
            'not_found' => __('No Lots found', 'html5blank'),
            'not_found_in_trash' => __('No Lots found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'author'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}

add_action('init', 'create_post_type_lot');



// Register Taxonomy location
// Taxonomy Key: location
function create_location_tax() {

    $labels = array(
        'name'              => _x( 'locations', 'taxonomy general name', 'textdomain' ),
        'singular_name'     => _x( 'location', 'taxonomy singular name', 'textdomain' ),
        'search_items'      => __( 'Search locations', 'textdomain' ),
        'all_items'         => __( 'All locations', 'textdomain' ),
        'parent_item'       => __( 'Parent location', 'textdomain' ),
        'parent_item_colon' => __( 'Parent location:', 'textdomain' ),
        'edit_item'         => __( 'Edit location', 'textdomain' ),
        'update_item'       => __( 'Update location', 'textdomain' ),
        'add_new_item'      => __( 'Add New location', 'textdomain' ),
        'new_item_name'     => __( 'New location Name', 'textdomain' ),
        'menu_name'         => __( 'Locations', 'textdomain' ),
    );
    $args = array(
        'labels' => $labels,
        'description' => __( 'Property Neighborhood', 'textdomain' ),
        'hierarchical' => false,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => false,
        'show_tagcloud' => true,
        'show_in_quick_edit' => true,
        'show_admin_column' => true,
    );
    register_taxonomy( 'location', array('buy', 'rent', 'newdev', 'first-home', 'lot' ), $args );

}
add_action( 'init', 'create_location_tax' );

// Register Property Type
// Taxonomy Key: type
function create_type_tax() {

    $labels = array(
        'name'              => _x( 'types', 'taxonomy general name', 'textdomain' ),
        'singular_name'     => _x( 'type', 'taxonomy singular name', 'textdomain' ),
        'search_items'      => __( 'Search types', 'textdomain' ),
        'all_items'         => __( 'All types', 'textdomain' ),
        'parent_item'       => __( 'Parent type', 'textdomain' ),
        'parent_item_colon' => __( 'Parent type:', 'textdomain' ),
        'edit_item'         => __( 'Edit type', 'textdomain' ),
        'update_item'       => __( 'Update type', 'textdomain' ),
        'add_new_item'      => __( 'Add New type', 'textdomain' ),
        'new_item_name'     => __( 'New type Name', 'textdomain' ),
        'menu_name'         => __( 'Property type', 'textdomain' ),
    );
    $args = array(
        'labels' => $labels,
        'description' => __( 'Property Neighborhood', 'textdomain' ),
        'hierarchical' => false,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => false,
        'show_tagcloud' => true,
        'show_in_quick_edit' => true,
        'show_admin_column' => true,
    );
    register_taxonomy( 'type', array('buy', 'rent', 'newdev', 'first-home', 'lot' ), $args );

}
add_action( 'init', 'create_type_tax' );

function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyBDmlahyk1NsYccoF0LG3hH5TKQdOL8chw');
}

add_action('acf/init', 'my_acf_init');

/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}

// Register Custom Navigation Walker
      require_once('bs4navwalker.php');


add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );
function my_show_extra_profile_fields( $user ) { ?>
<h3>Extra profile information</h3>
    <table class="form-table">
<tr>
            <th><label for="phone">Phone Number</label></th>
            <td>
            <input type="text" name="phone" id="phone" value="<?php echo esc_attr( get_the_author_meta( 'phone', $user->ID ) ); ?>" class="regular-text" /><br />
                <span class="description">Please enter your phone number.</span>
            </td>
</tr>
</table>
<?php }

add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );

function my_save_extra_profile_fields( $user_id ) {

if ( !current_user_can( 'edit_user', $user_id ) )
    return false;

update_usermeta( $user_id, 'phone', $_POST['phone'] );
}


//Filter by tax

function filter_cars_by_taxonomies( $post_type, $which ) {

    // Apply this only on a specific post type
    if ( 'buy' !== $post_type )
        return;

    // A list of taxonomy slugs to filter by
    $taxonomies = array( 'type', 'location' );

    foreach ( $taxonomies as $taxonomy_slug ) {

        // Retrieve taxonomy data
        $taxonomy_obj = get_taxonomy( $taxonomy_slug );
        $taxonomy_name = $taxonomy_obj->labels->name;

        // Retrieve taxonomy terms
        $terms = get_terms( $taxonomy_slug );

        // Display filter HTML
        echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
        echo '<option value="">' . sprintf( esc_html__( 'Show All %s', 'text_domain' ), $taxonomy_name ) . '</option>';
        foreach ( $terms as $term ) {
            printf(
                '<option value="%1$s" %2$s>%3$s (%4$s)</option>',
                $term->slug,
                ( ( isset( $_GET[$taxonomy_slug] ) && ( $_GET[$taxonomy_slug] == $term->slug ) ) ? ' selected="selected"' : '' ),
                $term->name,
                $term->count
            );
        }
        echo '</select>';
    }

}
add_action( 'restrict_manage_posts', 'filter_cars_by_taxonomies' , 10, 2);


// // Utm tracking
// $sources = array(
//     source => $_GET['utm_source'],
//     medium => $_GET['utm_medium'],
//     campaign => $_GET['utm_campaign'],
//     term => $_GET['utm_term'],
//     content =>  $_GET['utm_content'],
//     adwords =>  $_GET['gclid']
// );

// function bai_set_utm(){
//     $bai_utm = array(
//         source => $_GET['utm_source'],
//         medium => $_GET['utm_medium'],
//         campaign => $_GET['utm_campaign'],
//         term => $_GET['utm_term'],
//         content =>  $_GET['utm_content'],
//         adwords =>  $_GET['gclid']
//     );

//     if(isset($_COOKIE['bai_utm_params'])) {
//         function bai_get_utm_params(){
//             $bai_utm_cookies = $_COOKIE['bai_utm_params'];
//             return $bai_utm_cookies+"yes cookies";
//         }
//     } else {
//         function bai_get_utm_params(){
//             $bai_utm_cookies = $bai_utm;
//             return $bai_utm_cookies+"no cookies";
//         }
//         //setCookies
//         setcookie('bai_utm[source]',  $bai_utm['source'], time()+2592000);
//         setcookie('bai_utm[medium]',  $bai_utm['medium'], time()+2592000);
//         setcookie('bai_utm[campaign]',  $bai_utm['campaign'], time()+2592000);
//         setcookie('bai_utm[term]',  $bai_utm['term'], time()+2592000);
//         setcookie('bai_utm[content]',  $bai_utm['content'], time()+2592000);
//         setcookie('bai_utm[adwords]',  $bai_utm['adwords'], time()+2592000);
//     }
// }
// add_action('init', 'bai_set_utm');

// if (isset($_COOKIE['bai_utm'])) {
//     global $sources;
//     foreach ($_COOKIE['bai_utm'] as $name => $value) {
//         $name = htmlspecialchars($name);
//         $value = htmlspecialchars($value);
//          //echo "$name : $value <br />\n";
//         $utms[] = $value;
//     }
// }elseif(isset($sources)) {
//     foreach ($sources as $name => $value) {
//         $name = htmlspecialchars($name);
//         $value = htmlspecialchars($value);
//          //echo "$name : $value <br />\n";
//         $utms[] = $value;
//     }

// }
// //lead source referal
// if (isset($_SERVER['HTTP_REFERER'])){

//     if (!isset($_COOKIE['leadsource'])) {

//         $referer = $_SERVER['HTTP_REFERER'];
//         $cookie_expire = time()+2592000*2;//60 days

//         while (($data = $referer) !== FALSE)
//         {
//             if (strtoupper($data)==strtoupper($referer)){
//                 setcookie("leadsource", $data, $cookie_expire, "/");
//                 break;
//             }
//         }

//     }
// }

function add_custom_types_to_tax( $query ) {
if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
 
// Get all your post types
$post_types =  array('post', 'buy', 'newdev', 'rent', 'lot');
 
$query->set( 'post_type', $post_types );
return $query;
}
}
add_filter( 'pre_get_posts', 'add_custom_types_to_tax' );

?>
