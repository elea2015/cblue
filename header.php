<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
    

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cardo:400,700" rel="stylesheet"> 

    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/lightbox/css/lightbox.css">

    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/animate/animate.css">
    
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://use.fontawesome.com/44eb749b58.js"></script>

    
		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script> 
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/node_modules/owl.carousel/dist/assets/owl.carousel.min.css" />
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/node_modules/owl.carousel/dist/assets/owl.theme.default.min.css">

        <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MPN7QFF');</script>
<!-- End Google Tag Manager -->

	</head>
	<body <?php body_class(); ?>>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MPN7QFF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  
	<nav class="navbar fixed-top navbar-expand-lg navbar-light"><div class="container">
           <div class="topHeader text-right container">
            <ul class="list-inline">
              <li class="list-inline-item"><a target="_blank" href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwjUvMW39d3YAhUV9WMKHZIeBnoQFggnMAA&url=https%3A%2F%2Fwww.facebook.com%2Fcanablue%2F&usg=AOvVaw0KwCs4Y8hcQNZGUGpmFZyq"><i class="fa fa-facebook"></i></a></li>
              <li class="list-inline-item"><a target="_blank" href="https://www.instagram.com/cana_blue_punta_cana/"><i class="fa fa-instagram"></i></a></li>
				      <li class="list-inline-item"><a target="_blank" href="https://www.linkedin.com/company/canablue"><i class="fa fa-linkedin"></i></a></li>
              <li class="list-inline-item"><a href="tel:+1-809-975-3900"><i class="fa fa-mobile fa-lg"></i> +1-809-975-3900</a></li>
				
            </ul>
  </div>
        <a class="navbar-brand" href="<?php echo home_url(); ?>">
          <img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="Logo" class="logo-img logoDesktop">
          <img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="Logo" class="logo-img logoMobile">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <?php
         wp_nav_menu([
           'menu'            => 'top',
           'theme_location'  => 'header-menu',
           'container'       => 'div',
           'container_id'    => 'bs4navbar',
           'container_class' => 'navbar-nav m-auto',
           'menu_id'         => false,
           'menu_class'      => 'navbar-nav ml-auto',
           'depth'           => 2,
           'fallback_cb'     => 'bs4navwalker::fallback',
           'walker'          => new bs4navwalker()
         ]);
         ?>
        </div>
      </div></nav>

