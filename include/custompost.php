
<main role="main listing">
	<span style="display:none"><?php echo $adwords;?></span>
	<!-- section -->
	<section>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php 
			//Get post data
			$location = 	get_the_term_list( $post->ID, 'location'); // location
			$price = 		number_format(get_field('price'));
			$listingType = 	get_field('listing_type');
			$squaterMT = 	number_format(get_field('square_mt'));
			$squareFT =		number_format(get_field('square_mt') * 10.764, 2, '.', '');//number_format(get_field('square_ft'));
			$lotSize = 		number_format(get_field('lot_size'));
			$bathrooms = 	number_format(get_field('bathrooms'));
			$bedrooms = 	number_format(get_field('bedrooms'));
			$status = 		get_field('status');
			$yearBuilt =	get_field('year_built');
			$taxes = 		get_field('taxes');
			$realTaxes =	get_field('realTaxes');
			$houseRules =	get_field('house_rules');
			$time =			get_field('time');
			$onMarket =		get_field('days_on_the_market');
			$confotur = 	get_field('confotur');
			$brochure =		get_field('brochure');
			?>


			<!-- Listing content -->
			<section class="hero unitHero" style="background: url('<?php the_post_thumbnail_url();  ?>'); background-size:cover;">
				<img style="display: none;" src="<?php the_post_thumbnail_url();  ?>">
				<div class="opacityBg">
					<div class="container">
						<div class="row">
							<div class="col-md-9">
								<?php if(get_field('sale_status') == 'Sale Pending'): ?><span class="badge badge-warning">Sale Pending</span><br>
								<?php elseif(get_field('sale_status') == 'Sold'): ?><span class="badge badge-success">Sold</span><br>
								<?php elseif(get_field('sale_status') == 'Only several units left'): ?><span class="badge badge-warning">Only several units left</span><br>
								<?php elseif(get_field('sale_status') == 'Sold out'): ?><span class="badge badge-success">Sold out</span><br>
								<?php endif; ?>
								<?php if(get_field('rent_status') == 'Rented'): ?> <span class="badge badge-success">Rented</span><br><?php endif; ?>
								<span><?php the_tags( __( '', 'html5blank' ), ' | '); ?></span>
								<h1><?php the_title(); ?></h1>
								<p><i class="fa fa-map-marker"></i> <?php echo $location; ?></p>
								<h4>
									<?php 
									if (is_singular('newdev')){echo "Prices starting at ";}
									echo "$".$price;
									if (is_singular( 'rent' )){
										if ( $time == 'Month' ) {echo " /Month";} 
										elseif ( $time == 'Night' ) {echo " /Night";} 
										elseif ( $time == 'Week' ) {echo " /Week";} 
										else {echo "/From";} 
									}
									if (is_singular('lot')){echo "/ per square meter ";}
									?>
								</h4>
							</div>
							<div class="col-md-3">
								<div class="contactForm">
									<?php echo get_avatar( get_the_author_meta( 'ID' ),200); ?><br>
									<h5><?php the_author_meta( 'first_name'); ?> <?php the_author_meta( 'last_name'); ?></h5>
									<p><i class="fa fa-mobile"></i> +1-<?php the_author_meta( 'phone'); ?> </p>
									<a href="#"><i class="fa fa-envelope-o"></i> <?php the_author_meta( 'user_email'); ?></a>
									<?php
									$property_field = get_the_title();
									echo do_shortcode('[gravityform id="2" field_values="property=' .$property_field. '" title="false" description="false"]');
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section> 

			<section class="unitDetails">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<div class="gallery">
							<!-- gallery -->
							<?php 
							$images = get_field('gallery');
							$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
							?>
							<div class="owl-carousel owl-theme">
								<?php if( $images ): ?>
							        <?php foreach( $images as $image ): ?>
							            <div class="item">
							            	<a href="<?php echo wp_get_attachment_url( $image['ID'] ); ?>" data-lightbox="image-1" >
							            		<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
							            	</a>
							            </div>
							        <?php endforeach; ?>
								<?php endif; ?>
							</div>
							</div>
							<?php if($brochure): ?>
							<div class="brochure">
								<?php echo do_shortcode('[et_bloom_locked optin_id="optin_1"]<a href="'.$brochure.'" target="_blank" class="btn btn-primary">Download Brochure</a>[/et_bloom_locked]') ?>
							</div>
							<?php endif; ?>
							
							<br><br>
							<?php if($confotur):?>
							<div class="row">
								<div class="col">
									<style type="text/css">
									.tooltip-inner{
										min-width: 370px;
									}
									</style>
									<h5>Confotur (Tax Free) <i class="fa fa-question-circle" data-toggle="tooltip" data-html="true" title="<p class='text-left'>CONFOTUR is a tourism incentive law in the Dominican Republic with the sole purpose to increase foreign investment in touristic areas such as Punta Cana. For buyers and investors in Punta Cana, purchasing a condo with CONFOTUR means being free of transfer tax (3%) and from property tax (1%) for 15 years.</p>"></i></h5>
								</div>
							</div>
							<?php endif?>
							<!-- <div class="row saleBenefits">
								<div class="col-md-6">
									<h4 class="taxes">
										<?php //if( $taxes): ?>
										<?php //echo $taxes;?>% Tax
										<?php //endif; ?>
									</h4>
								</div>
								<div class="col-md-6">
									<h4 class="taxes">
										<?php //if( $realTaxes ): ?>
										<?php //echo $realTaxes ;?>% Real Estate tax (IPI)
										<?php //endif; ?>
									</h4>
								</div>
							</div> -->
							<br><br>
							<!-- Main info -->
							<div class="row mainInfo">
								<div class="col">
									<h3>Main Information</h3>
									<?php if(!is_singular('lot')):?>
									<table class="table">
									  <tbody>
									    <tr>
									      <th>Listing Type</th>
									      <td><?php echo $listingType->name; ?></td>
									      <?php if($listingType->name !== 'Commercial'): ?>
									      <th>Bedrooms</th>
									      <td><?php echo $bedrooms; ?></td>
									  <?php endif; ?>
									    </tr>
									    <tr>
									      <th>Square FT</th>
									      <td><?php echo $squareFT; ?></td>
									      <?php if($listingType->name !== 'Commercial'): ?>
									      <th>BathRoom</th>
									      <td><?php echo $bathrooms; ?></td>
									  <?php endif; ?>
									    </tr>
									    <tr>
									      <th>Square MT</th>
									      <td><?php echo $squaterMT; ?></td>
									      <?php if (is_singular( 'buy' )): ?>
									      <th>Status</th>
									      <td><?php echo $status; ?></td>
									  		<?php endif ?>
									    </tr>
									    <?php if ($lotSize):?>
									    <tr>
									      <th>Lot Size MT</th>
									      <td><?php echo $lotSize; ?></td>
									    </tr>
									    <?php endif;?>
								
									  </tbody>
									</table>
									<?php endif;?>
									
									<?php if(is_singular('lot')):?>
									<table class="table">
									  <tbody>
									    <tr>
									      <th>Square FT</th>
									      <td><?php echo $squareFT; ?></td>
									      <th>Square MT</th>
									      <td><?php echo $squaterMT; ?></td>
									    </tr>
						
									  </tbody>
									</table>
									<?php endif;?>
									<br><br>
									<div class="unitDescroption">
										<h3>Description</h3>
										<?php the_content(); // Dynamic Content ?>
									</div>
									<br>
									<div class="unitDescroption">
										<?php if($houseRules): ?><h3>House Rules</h3><?php endif;?>
										<?php echo $houseRules; // Dynamic Content ?>
									</div>
								</div>
							</div>
							<br>
						</div>
					</div>
				</div>
			</section>
			<br>
			<br>
			<div class="container">
				<div class="row">
					<div class="col-6">
						<?php //echo do_shortcode('[et_bloom_locked optin_id="optin_1"]Hola[/et_bloom_locked]') ?>
					</div>
				</div>
			</div>
			<?php get_template_part('include/map');?>
			<section class="unitMap">
				<div class="container">
					<div class="row">
						<div class="col">
							<h3>Point of Interest</h3>
							<?php 

							$location = get_field('map');

							if( !empty($location) ):
							?>
							<div class="acf-map">
								<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
							</div>
						<?php endif; ?>
						</div>
					</div>
				</div>

				<div class="contactFormMobile">
					<?php echo get_avatar( get_the_author_meta( 'ID' ),200); ?><br>
									<h5><?php the_author_meta( 'first_name'); ?> <?php the_author_meta( 'last_name'); ?></h5>
									<p><i class="fa fa-mobile"></i> +1-<?php the_author_meta( 'phone'); ?> </p>
									<a href="#"><i class="fa fa-envelope-o"></i> <?php the_author_meta( 'user_email'); ?></a>
					<?php
					$property_field = get_the_title();
					echo do_shortcode('[gravityform id="2" field_values="property='.$property_field.'" title="false" description="false"]');
					?>
				</div>
			</section>
		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php endif; ?>

	</section>
	<!-- /section -->
	</main>