<section class="aboutPamela">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<h4>About Pamela</h4>
					<p>Born and raised in Santo Domingo, Dominican Republic, she started her career in real estate in Santo Domingo 10 years ago. Graduated from law school in the year 2010 from the Pontificia Universidad Catolica Madre y Maestra and once then, seeking greater business opportunities, Punta Cana called her attention in 2012.</p>
					<p>Punta Cana has been her home for the past 6 years working closely with foreign and domestic investors in the area. She is a Real Estate professional with a demonstrated history of working in the real estate and legal industry. Skilled in Negotiation, Legal Affairs, Customer Acquisition, Customer Relationship Management, and Sales.</p>
					<p>Pamela is glad to be able to offer her clients a full cycle service range including real estate promotions and sales, real estate legal assistance and general legal assistance, as well as  renovations and furnishing options with her full service team in the real estate industry.</p>
				</div>
				<div class="col-md-4 wow fadeInRight">
					<img src="<?php echo get_template_directory_uri(); ?>/img/pamelabio.png">
				</div>
			</div>
		</div>
	</section>