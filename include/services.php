<section class="homeSection ourServices wow fadeInUp">
		<div class="container">
			<div class="row text-center">
				<div class="col">
					<h2>Our Services</h2>
					<br>
				</div>
			</div>
			<div class="row text-center grid">
				<div class="col-md-6">
					<a href="<?php echo home_url(); ?>/?page_id=349">
						<div class="buy">
							<h5>Buy</h5>
							<hr>
							<p>New Constructions | Re-Sales | Investment Opportunities</p>
						</div>
					</a>
				</div>
				<div class="col-md-6">
					<a href="<?php echo home_url(); ?>/?page_id=106">
						<div class="legal">
							<h5>Legal</h5>
							<hr>
							<p>Real Estate Development | CONFOTUR | Legal Assistance and Consultation</p>
						</div>
					</a>
				</div>
			</div>

			<div class="row text-center grid">
				<div class="col-md-6">
					<a href="<?php echo home_url(); ?>/?page_id=94">
						<div class="sell">
							<h5>Sell</h5>
							<hr>
							<p>Rent | Property Managment</p>
						</div>
					</a>
				</div>
				<div class="col-md-6">
					<a href="#">
						<div class="rentals">
							<h5>Rentals</h5>
							<hr>
							<p>Long Term (Coming Soon)</p>
						</div>
					</a>
				</div>
				<div style="display: none" class="col-md-4">
					<a href="<?php echo home_url(); ?>/?page_id=108">
						<div class="renovations">
							<h5>Numar Design Studio and Projects</h5>
							<hr>
							<p>Renovations | Turn Key Packages | Furnishing Options | Construction</p>
						</div>
					</a>
				</div>
			</div>
		</div>
	</section>