<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
  </ol>
  <div class="carousel-inner">
<?php 
$c = 0;
$class = '';
if( have_rows('slider') ): ?>

  <?php while( have_rows('slider') ): the_row(); $c++;

    // vars
    $project_name = get_sub_field('project_name');
    $project_copy = get_sub_field('project_copy');
    $project_link = get_sub_field('project_link');
    $project_image = get_sub_field('project_image');
    $size = 'slider-size';
    
    if ( $c == 1 ){ $class = ' active';}
else{ $class='';} 
    ?>
    <div class="carousel-item <?php echo $class;?>">
      <img class="d-block w-100" src="<?php echo wp_get_attachment_image_src( $project_image, $size )[0]; ?>" alt="First slide">
      <div class="carousel-caption d-md-block">
        <h2><?php echo $project_name; ?> </h2>
        <p><?php echo $project_copy; ?></p>
        <a class="btn btn-primary" href="<?php echo $project_link; ?>">Learn More</a>
      </div>
      <div class="sliderGradient"></div>
    </div>
  <?php endwhile; ?>

<?php endif; ?>

  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>