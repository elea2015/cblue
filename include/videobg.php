<section class="hero heroHome heroSell heroVideo" style="background:url(<?php the_field('ownersBg')?>);background-size:cover; background-position:center;">
		<video id="homeVideo" playsinline autoplay muted loop>
			<source src="<?php the_field('video') ?>" type="video/mp4">
		</video>
		<div class="container">
			<div class="row">
				<div class="col">
					<h1><?php the_field('service_hero'); ?></h1>
					<p><?php the_field('service_hero_pitch');?></p>
				</div>
			</div>
		</div>
		<img class="mouseIcon" src="<?php echo get_template_directory_uri(); ?>/img/icon/mouse.png">
	</section>