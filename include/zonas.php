<!-- Neiboorhoods -->
	<section class="homeSection ourServices"> <!-- wow fadeInUp -->
		<div class="container">
			<div class="row text-center">
				<div class="col">
					<h2>Neighborhoods</h2>
					<p class="px-5">We are re-defining real estate in each area. Discover our hand-picked properties: From golf to beach views, including flourishing neighborhoods and ROI real estate opportunities. </p>
					<br>
				</div>
			</div>
			<div class="row text-center grid ourZonas">
				<div class="col-md-4">
					<a href="<?php echo home_url();?>/location/palma-real-villas-cocotal">
						<div class="cocotal">
							<h5>Palma Real Villas (Cocotal)</h5>
							<hr>
							<p></p>
						</div>
					</a>
				</div>
				<div class="col-md-4">
					<a href="<?php echo home_url();?>/location/costa-bavaro">
						<div class="costaBavaro">
							<h5>Costa Bavaro</h5>
							<hr>
							<p></p>
						</div>
					</a>
				</div>
				<div class="col-md-4">
					<a href="<?php echo home_url();?>/location/cap-cana">
						<div class="capCana">
							<h5>Cap Cana</h5>
							<hr>
							<p></p>
						</div>
					</a>
				</div>
			</div>
		
			<div class="row text-center grid ourZonas">
				<div class="col-md-4">
					<a href="<?php echo home_url();?>/location/el-cortecito">
						<div class="cortecito">
							<h5>El Cortecito</h5>
							<hr>
							<p></p>
						</div>
					</a>
				</div>
				<div class="col-md-4">
					<a href="<?php echo home_url();?>/location/punta-cana">
						<div class="puntaCana">
							<h5>Punta Cana</h5>
							<hr>
							<p></p>
						</div>
					</a>
				</div>
				<div class="col-md-4">
					<a href="<?php echo home_url();?>/location/white-sands">
						<div class="whiteSands">
							<h5>White Sands</h5>
							<hr>
							<p></p>
						</div>
					</a>
				</div>
			</div>
		</div>
	</section>