<?php /* Template Name: blog*/ get_header(); ?>

	<section class="buySection d-none">
		<div class="container text-center">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col">
					<h2><?php the_title() ?></h2>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</section>
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<section class="buySection buyListing mt-5">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<?php
						$orig_query = $wp_query;
					
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						//Query Arguments
						$args = array(
							'post_type' => array('post'),
							'posts_per_page' => 18,
					        		'paged' => $paged,
					        		'orderby' => 'date',
					        		'order' => 'DESC'
						);
						$wp_query = new WP_Query($args); 
					?>
				
					<?php if($wp_query->have_posts()) ?>
					   <div class="">
					     <?php  while ( $wp_query->have_posts() ): $wp_query->the_post(); $a++;?>
					        	<?php 
					        		$propertyLink = get_permalink();
					        		$propertyId =	get_the_ID();
					
					        		$time = get_field('time');
					
					        		//Get Firs tag
					        		$allposttags = get_the_tags();
					                $i=0;
					                if ($allposttags) {
					                    foreach($allposttags as $tags) {
					                        $i++;
					                        if (1 == $i) {
					                            $firsttag = $tags->name;
					                        }
					                    }
					                };
					                $categories = get_the_category($propertyId);
					                $catname = $categories[0]->name; 
					        	 ?>
					          	<!-- Price Card -->
					          	<style>
									
										
										.card-img-top-container{
											height: initial !important;
										}

										.card-img-top{
											height: initial !important;
										}

									

									.btn{
											color:white !important;
										}
					          	</style>
								<div class="card <?php echo $locationGrid; ?>">
									
										<a href="<?php echo $propertyLink; ?>">
											<div class="card-img-top-container">
												<img class="card-img-top" src="<?php the_post_thumbnail_url('custom-size'); ?>" alt="Card image cap">
											</div>
										</a>
										<h5><span class="badge badge-primary"><?php echo $catname; ?></span></h5>
										<div class="card-body">
											<a href="<?php echo $propertyLink; ?>"><h4 class="card-title"><?php the_title(); ?></h4></a>
											<!-- <h6 class="card-subtitle mb-2 text-muted location"><?php// echo get_the_term_list( $propertyId, 'location'); ?></h6> -->
											<p><?php the_excerpt();?></p>
											<a class="btn btn-primary" href="<?php echo $propertyLink; ?>">Read more </a>
										</div>
									
								</div>
								<!-- end price Card -->
					     <?php  //if($a % 3 === 0) :  echo '</div> <div class="row priceRow">'; endif; ?>
						<?php endwhile; ?>
						</div>
					
						<div class="nav-previous alignleft"><?php next_posts_link( 'Older posts' ); ?></div>
						<div class="nav-next alignright"><?php previous_posts_link( 'Newer posts' ); ?></div>
						<?php wp_reset_query(); ?> 
					</div>
					<div class="col-md-4 sidebar border-left">
				<div class="pl-2">
					<?php 
						get_sidebar();
						echo '<br>';
						get_template_part('include/feature-blog');
					 ?>

				</div>
			</div>
				</div>
			</div>
		</div>
	</section>

	<?php endwhile; endif;  $wp_query = $orig_query; ?>

	<?php 
	get_template_part('include/optin');
	?>

	<?php get_template_part('include/zonas')?>

<?php get_footer(); ?>