<?php /* Template Name: Landing home */ get_header(); ?>

	<section class="hero heroHome heroSell heroVideo" style="background:url(<?php the_field('landingBg')?>);background-size:cover; background-position:center;">
		<video id="homeVideo" playsinline autoplay muted loop>
			<source src="<?php the_field('video') ?>" type="video/mp4">
		</video>
		<div class="container">
			<div class="row">
				<div class="col">
					<h1>PUNTA CANA REAL ESTATE</h1>
					<p>Canablue is a real estate investment firm targeted to foreign and domestic investors or second home seekers, looking to find the perfect real estate in Punta Cana</p>
				</div>
			</div>
		</div>
		<img class="mouseIcon" width="40px" src="<?php echo get_template_directory_uri(); ?>/img/icon/mouse.png">
	</section>
	
	<?php get_template_part('include/optin'); ?>

	<div class="container searchForm my-4"><?php echo do_shortcode( '[searchandfilter add_search_param="1" fields="location,type"  post_types="buy,newdev,first-home,rent" submit_label="Search" all_items_labels="All Locations, All Types"]' ); ?></div>
	
	<div class="container mt-4">
		<div class="row">
			<div class="col-md-12"><?php get_template_part('include/slider'); ?></div>
		</div>
	</div>

	<section class="discoverHow homeSection">
		<div class="container text-center">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col">
					<h2><?php the_field('discover_heading');?></h2>
					<p><?php //the_field('discover_text');?></p>
					<a href="<?php echo site_url();?>/?page_id=10" class="btn btn-primary">Discover Now</a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</section>

	<section class="container my-5"><?php get_template_part('include/feature') ?></section>



	<?php get_template_part('include/zonas')?>
	
	<!-- Services -->
	<?php get_template_part('include/services'); ?>
	
	<!-- About Pamela -->
	<?php get_template_part('include/pamela'); ?>

	<?php if ( is_user_logged_in() ) : ?>
	<style type="text/css">
		#testimonials .row{
			justify-content:center;
		}
		.testimonial_rotator_star{
			color:#1b4073;
		}
		.testimonial_rotator_slide_title{
			font-size: 18px;
			color: #1b4073;
		}
	</style>
	<section id="testimonials" class="testimonials pt-5">
		<div class="container">
			<div class="row justify-contnent-center">
				<div class="col-md-10">
					<h1 class="mb-3 text-center blue">Testimonials</h1>
					<?php echo do_shortcode('[testimonial_rotator id=2834]'); ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>	

<?php get_footer(); ?>
