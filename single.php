<?php get_header(); ?>

	<style>
		main{
			padding-top: 50px;	
		}
	</style>

	<main role="main">
	<!-- section -->
	<section class="container">

		<div class="row">
			<div class="col-md-8">
				<?php if (have_posts()): while (have_posts()) : the_post(); ?>
				
					<!-- article -->
					<article class="pr-2" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<h1 class="blue mb-2"><?php the_title(); ?></h1>
						<!-- post details -->
						<span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
						<!-- /post details --><br><br>
				
						<!-- post thumbnail -->
						<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
								<?php the_post_thumbnail('property-screenshot'); // Fullsize image for the single post ?>
						<?php endif; ?>
						<!-- /post thumbnail -->
						<br><br>
				
						<?php the_content(); // Dynamic Content ?>
				
						<?php //the_tags( __( 'Tags: ', 'html5blank' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>
				
						<p><?php //_e( 'Categorised in: ', 'html5blank' ); the_category(', '); // Separated by commas ?></p>
				
						<p><?php //_e( 'This post was written by ', 'html5blank' ); the_author(); ?></p>
				
						<?php edit_post_link(); // Always handy to have Edit Post Links available ?>
				
						<?php //comments_template(); ?>
				
					</article>
					<!-- /article -->
				
				<?php endwhile; ?>
				
				<?php else: ?>
				
					<!-- article -->
					<article>
				
						<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>
				
					</article>
					<!-- /article -->
				
				<?php endif; ?>
			</div>
			<div class="col-md-4 sidebar border-left">
				<div class="pl-2">
					<?php 
						get_sidebar();
						echo '<br>';
						get_template_part('include/feature-blog');
					 ?>

				</div>
			</div>
		</div>

	</section>
	<!-- /section -->
	</main>

<?php get_footer(); ?>
