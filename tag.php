<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section style="margin:100px auto" class="container">
			<div class="row">
				<div class="col-md-8">
					<h1><?php _e( 'Tag Archive: ', 'html5blank' ); echo single_tag_title('', false); ?></h1>
					<?php get_template_part('loop'); ?>
					<?php get_template_part('pagination'); ?>
				</div>
				<div class="col-md-4"><?php get_sidebar(); ?></div>
			</div>
		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
